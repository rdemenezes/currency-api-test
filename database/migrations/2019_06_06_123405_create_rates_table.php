<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('rates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('base_id');
            $table->foreign('base_id')->references('id')->on('bases');
            $table->integer('target_base_id');
            $table->foreign('target_base_id')->references('id')->on('bases');
            $table->unique(['base_id', 'target_base_id']);
            $table->float('rate', 9, 2)->unsigned();
            $table->date('date')->default(date('Y-m-d'));
            $table->timestamp('created_at')->default(date('Y-m-d H:i:s'));
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('rates');
    }
}
