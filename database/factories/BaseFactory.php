<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Base;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Base::class, function (Faker $faker) {
    return [
        'base_code' => strtoupper(Str::random(3)),
        'base_name' => Str::random(10)
    ];
});
