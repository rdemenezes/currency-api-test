<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Rate;
use App\Base;
use Faker\Generator as Faker;

$factory->define(Rate::class, function (Faker $faker) {
    $baseIds = Base::all()->pluck('id')->toArray();
    [$baseId, $targetBaseId] = $faker->unique()->randomElements($baseIds, 2);

    return [
        'base_id' => $baseId,
        'target_base_id' => $targetBaseId,
        'rate' => $faker->randomFloat(5, 0, 100)
    ];
});
