<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

class RateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            "id" => $this->id,
            "baseId" => $this->base_id,
            "targetBaseId" => $this->target_base_id,
            "rate" => $this->rate,
            "date" => $this->date,
            "updatedAt" => $this->updated_at
        ];
    }
}
