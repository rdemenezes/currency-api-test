<?php
namespace App\Http\Controllers;

use App\Rate;
use App\Exceptions\ResourceAlreadyExistsException;
use App\Http\Resources\RateResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection as Collection;
use Illuminate\Support\Facades\DB;

class RatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Collection
     */
    public function index(): Collection
    {
        return RateResource::collection(Rate::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @throws ResourceAlreadyExistsException
     * @return RateResource
     */
    public function store(Request $request)
    {
        $data = $this->validateRequestData($request);

        if ($this->rateExists($data['base_id'], $data['target_base_id'])) {
            throw new ResourceAlreadyExistsException();
        }

        $rate = new Rate($data);
        if ($rate->save()) {
            return new RateResource($rate->fresh());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return RateResource
     */
    public function update(Request $request): RateResource
    {
        $baseId = $request->route('base_id');
        $targetBaseId = $request->route('target_base_id');
        $data = $this->validateRequestData($request);
        $rate = $this->getRate($baseId, $targetBaseId);

        if (is_null($rate)) {
            throw new ModelNotFoundException();
        }

        $rate->update($data);
        if ($rate->save()) {
            return new RateResource($rate->fresh());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $base_id
     * @throws ModelNotFoundException
     * @return Collection
     */
    public function show($base_id): Collection
    {
        $rates = DB::table('rates')->where('base_id', $base_id)->get();
        if ($rates->isEmpty()) {
            throw new ModelNotFoundException();
        }
        return RateResource::collection($rates);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RateResource
     */
    public function destroy($id): RateResource
    {
        $rate = Rate::findOrFail($id);

        if ($rate->delete()) {
            return new RateResource($rate);
        }
    }

    /**
     * Checks if a rate for given base id and target base id exists
     * @return bool
     */
    protected function rateExists($baseId, $targetBaseId): bool
    {
        if (
            Rate::all()
                ->where('base_id', $baseId)
                ->where('target_base_id', $targetBaseId)
                ->isNotEmpty()
        ) {
            return true;
        }
        return false;
    }

    /**
     * Gets the rate for a given base id and target base id
     * @return Rate|null
     */
    protected function getRate($baseId, $targetBaseId): ?Rate
    {
        $rate = Rate::all()
            ->where('base_id', $baseId)
            ->where('target_base_id', $targetBaseId);

        return $rate->first();
    }

    /**
     * Validate the data from the request
     *
     * @param Request $request
     * @return array
     */
    private function validateRequestData(Request $request): array
    {
        $validatedData = request()->validate([
            'base_id' => [
                'required',
                'integer',
                'min:0',
                'unique_with:bases,target_base_id',
                'different:target_base_id'
            ],
            'target_base_id' => ['required', 'integer', 'min:0'],
            'rate' => ['required', 'numeric', 'min:0'],
            'date' => ['date', 'before:tomorrow', 'date_format:Y-m-d']
        ]);

        return $validatedData;
    }
}
