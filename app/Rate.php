<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Rate extends Model
{
    /**
     * Allow fields to be passed into the object
     */
    protected $fillable = ['base_id', 'target_base_id', 'rate', 'date'];

    /**
     * Format the outputs
     */
    protected $casts = [
        'base_id' => 'integer',
        'target_base_id' => 'integer',
        'rate' => 'float',
        'updated_at' => 'string'
    ];
}
