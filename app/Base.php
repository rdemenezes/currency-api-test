<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Base extends Model
{
    public $timestamps = false;

    /**
     * Allow fields to be passed into the object
     */
    protected $fillable = ['base_code', 'base_name'];
}
