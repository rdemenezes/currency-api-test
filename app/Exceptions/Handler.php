<?php
namespace App\Exceptions;

use Exception;
use App\Exceptions\ResourceAlreadyExistsException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof ModelNotFoundException) {
            return $this->buildErrorResponse(
                Response::HTTP_NOT_FOUND,
                'Resource not found.'
            );
        } else if ($exception instanceof NotFoundHttpException) {
            return $this->buildErrorResponse(
                Response::HTTP_NOT_FOUND,
                'Route incorrect.'
            );
        }  else if ($exception instanceof ResourceAlreadyExistsException) {
            return $this->buildErrorResponse(
                Response::HTTP_NOT_FOUND,
                'Resource already exists.'
            );
        }

        return parent::render($request, $exception);
    }

    private function buildErrorResponse($status, $message)
    {
        return response()->json([
            'message' => $message
        ], $status);
    }
}
