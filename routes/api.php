<?php

Route::get('rates', 'RatesController@index');
Route::post('rates', 'RatesController@store');

Route::put('rates/{base_id}/{target_base_id}', 'RatesController@update');

Route::get('rates/{base_id}', 'RatesController@show');
Route::delete('rates/{base_id}', 'RatesController@destroy');
